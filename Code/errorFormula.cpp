#include <medusa/Medusa.hpp>
#include <Eigen/Sparse>
#include <iostream>

using namespace mm;
using namespace Eigen;






int factorial(int n){
    if (n<=1) return 1;
    return n*factorial(n-1);
}

long double f2(long double x, long double y){
    return 1 + std::sin(4*x)+std::cos(3*x)+std::sin(2*y);
}
long double deryf2(long double x, long double y, int k){ //k-th y derivative (k>0)
    int factor = pow(2,k);
    switch (k%4){
        case 0:
            return factor*std::sin(2*y);
        case 1:
            return factor*std::cos(2*y);
        case 2:
            return -factor*std::sin(2*y);
        case 3:
            return -factor*std::cos(2*y); 
    }
}

long double derxf2(long double x, long double y, int k){ //k-th x derivative (k>0)
    int factor1 = pow(4,k);
    int factor2 = pow(3,k);
    switch (k%4){
        case 0:
            return factor1*std::sin(4*x) + factor2*std::cos(3*x);
        case 1:
            return factor1*std::cos(4*x) - factor2*std::sin(3*x);
        case 2:
            return -factor1*std::sin(4*x) - factor2*std::cos(3*x);
        case 3:
            return -factor1*std::cos(4*x) + factor2*std::sin(3*x);
    }
}

long double derkf2(long double x, long double y, int k1, int k2){ // apply operator Lk
    if (k1 == 0){
        return deryf2(x,y,k2)/factorial(k2);
    } else if (k2 == 0){
        return derxf2(x,y,k1)/factorial(k1);
    } else {
        return 0;
    }
    //assuming k1=k2=0 doesn't happen
}

long double lapf2(long double x, long double y){
    return -16*std::sin(4*x) - 9*std::cos(3*x)-4*std::sin(2*y);
}

void solve(int m, int seed, int k){
    long double eps = 1e-10; //for floating comparison
    std::ostringstream strs;
    strs << "Data/errorFormulam" << m << "k" << k << "Seed" << seed << ".h5";
    HDF hdf_out(strs.str(), HDF::DESTROY);
    Range<double> errors;
    Range<double> errorsLocal;
    BallShape<Vec2d> domain({0,0}, 1);
    Range<double> dxs;
    for (double dx = 0.002; dx <= 0.1; dx+=0.001){
        dxs.push_back(dx);
        auto discretization = domain.discretizeBoundaryWithStep(dx);
        GeneralFill<Vec2d> fill_engine;
        fill_engine.seed(seed);
        discretization.fill(fill_engine, dx);
        int n = (m+1)*(m+2); //Bayona recommendation
        FindClosest find_support(n);
        discretization.findSupport(find_support);
        int N = discretization.size();
        Monomials<Vec2d> mon(m);
        RBFFD<Polyharmonic<double,3>,Vec2d,ScaleToFarthest> approx({},mon);
        auto storage = discretization.computeShapes(approx);

        VectorXd localErrors(N); localErrors.setZero();
        for (int i : discretization.interior()){
            //Vsota po vseh k-tih monomih L_k(f(x0))*p_k^T * w   
            double localErrorX=0;
            double localErrorY=0;
            auto weight = storage.laplace(i);
            auto support = discretization.supportNodes(i);
            for (int j=0;j<n;++j){
                localErrorX+= weight(j)*pow(support[j](0)-support[0](0),k);
                localErrorY+= weight(j)*pow(support[j](1)-support[0](1),k);
            }
            localErrors(i) = derkf2(support[0](0),support[0](1),k,0)*localErrorX + derkf2(support[0](0),support[0](1),0,k)*localErrorY;
        }


        SparseMatrix<double, Eigen::RowMajor> M(N,N);
        VectorXd rhs(N); rhs.setZero();
        auto op = storage.implicitOperators(M,rhs);
        for (int p : discretization.interior()){
            op.lap(p) = localErrors(p);
        }
        for (int p : discretization.boundary()){
            op.value(p) = 0.0;
        }
        Eigen::BiCGSTAB<decltype(M), IncompleteLUT<double>> solver;
        solver.preconditioner().setDroptol(1e-5);
        solver.preconditioner().setFillfactor(50);
        solver.setMaxIterations(200);
        solver.setTolerance(1e-14);
        solver.compute(M);
        VectorXd u = solver.solve(rhs);

        errors.push_back(u.lpNorm<1>()/discretization.interior().size());
        errorsLocal.push_back(localErrors.lpNorm<1>()/discretization.interior().size());
    }
    hdf_out.writeDoubleArray("result", errors);
    hdf_out.writeDoubleArray("resultLocal",errorsLocal);
    hdf_out.writeDoubleArray("dx",dxs);
    hdf_out.closeFile();
}

int main(int argc, char *argv[]){
    int m = std::stoi(argv[1]);
    int seed = std::stoi(argv[2]);
    for (int k=1;k<=3;++k) solve(m,seed,m+k);
    return 0;
}