#include <medusa/Medusa.hpp>
#include <Eigen/Sparse>
#include <iostream>

using namespace mm;
using namespace Eigen;

void solve() {
    double dx = 0.01;
    int seed = 1;
    std::ostringstream strs;

    BallShape<Vec2d> ball({0,0}, 1.0);

    DomainDiscretization<Vec2d> domain = ball.discretizeBoundaryWithStep(dx);
    GeneralFill<Vec2d> fill_engine;
    fill_engine.seed(seed);
    domain.fill(fill_engine, dx);

    strs.str("");
    strs << "Data/discretisation.h5";
    HDF hdf_out(strs.str(), HDF::DESTROY);
    hdf_out.writeDouble2DArray("positions",domain.positions());
    hdf_out.close();
}

int main(){
    solve();
    return 0;
}
