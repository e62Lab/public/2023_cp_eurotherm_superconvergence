#!/bin/bash

# make the Data folder if it doesn't exist yet.
if [ ! -d "./Data" ]
then
echo Making the Data directory...
mkdir Data
fi

echo Running all the scripts...

for filename in discretisation.cpp
do
echo Compiling $filename...
filenameBase=$(basename $filename .cpp)
g++ -o $filenameBase "${filenameBase}.cpp" -O3 -I ../medusa/include -I /usr/include/hdf5/serial -L ../medusa/bin/ -L /usr/lib/x86_64-linux-gnu/hdf5/serial/ -l medusa_standalone -l hdf5 -Wall #Modify appropriately
echo Running it...
./$filenameBase
rm $filenameBase
done

for filename in convergence.cpp convergenceR.cpp  #m 2-5, seed 0-29 
do
echo Compiling $filename...
filenameBase=$(basename $filename .cpp)
g++ -o $filenameBase "${filenameBase}.cpp" -O3 -I ../medusa/include -I /usr/include/hdf5/serial -L ../medusa/bin/ -L /usr/lib/x86_64-linux-gnu/hdf5/serial/ -l medusa_standalone -l hdf5 -Wall #Modify appropriately
echo Running it...
for m in {2..5}
do
for seed in {0..29}
do
./$filenameBase $m $seed
done
done
rm $filenameBase
done

for filename in errorFormula.cpp  #m 2, seed 0-29 
do
echo Compiling $filename...
filenameBase=$(basename $filename .cpp)
g++ -o $filenameBase "${filenameBase}.cpp" -O3 -I ../medusa/include -I /usr/include/hdf5/serial -L ../medusa/bin/ -L /usr/lib/x86_64-linux-gnu/hdf5/serial/ -l medusa_standalone -l hdf5 -Wall #Modify appropriately
echo Running it...
for seed in {0..29}
do
./$filenameBase 2 $seed
done
rm $filenameBase
done

echo Running the Python script...
python3 generatePlots.py