#include <medusa/Medusa.hpp>
#include <Eigen/Sparse>
#include <iostream>

using namespace mm;
using namespace Eigen;

long double f2(long double x, long double y){
    return 1 + std::sin(4*x)+std::cos(3*x)+std::sin(2*y);
}
long double lapf2(long double x, long double y){
    return -16*std::sin(4*x) - 9*std::cos(3*x)-4*std::sin(2*y);
}


void solve(int m, int seed){
    long double eps = 1e-10; //for floating comparison
    std::ostringstream strs;
    strs << "Data/solm" << m << "Seed" << seed << ".h5";
    HDF hdf_out(strs.str(), HDF::DESTROY);
    Range<double> errors1, errorsInf;
    BallShape<Vec2d> domain({0,0}, 1);
    Range<double> dxs;
    for (double dx = 0.002; dx <= 0.1; dx+=0.001){
        dxs.push_back(dx);
        auto discretization = domain.discretizeBoundaryWithStep(dx);
        GeneralFill<Vec2d> fill_engine;
        fill_engine.seed(seed);
        discretization.fill(fill_engine, dx);
        int n = (m+1)*(m+2); //Bayona recommendation
        FindClosest find_support(n);
        discretization.findSupport(find_support);
        int N = discretization.size();
        Monomials<Vec2d> mon(m);
        RBFFD<Polyharmonic<double,3>,Vec2d,ScaleToFarthest> approx({},mon);
        auto storage = discretization.computeShapes(approx);
        SparseMatrix<double, Eigen::RowMajor> M(N,N);
        VectorXd rhs(N); rhs.setZero();
        auto op = storage.implicitOperators(M,rhs);
        for (int p : discretization.interior()){
            double x = discretization.pos(p,0);
            double y = discretization.pos(p,1);
            op.lap(p) = lapf2(x,y);
        }
        for (int p : discretization.boundary()){
            double x = discretization.pos(p,0);
            double y = discretization.pos(p,1);
            op.value(p) = f2(x,y);
        }
        //Eigen::SparseLU<decltype(M), COLAMDOrdering<int>> solver;
        Eigen::BiCGSTAB<decltype(M), IncompleteLUT<double>> solver;
        solver.preconditioner().setDroptol(1e-5);
        solver.preconditioner().setFillfactor(50);
        solver.setMaxIterations(200);
        solver.setTolerance(1e-14);
        solver.compute(M);
        VectorXd u = solver.solve(rhs);
        if (solver.info() != Eigen::ComputationInfo::Success)
            std::cout << "Solver didn't converge!" << std::endl;
        VectorXd localErrors(N); localErrors.setZero();
        double error=0;
        for (int i : discretization.interior()){
            double x = discretization.pos(i,0);
            double y = discretization.pos(i,1);
            localErrors(i) = u(i)-f2(x,y);
        }
        errorsInf.push_back(localErrors.lpNorm<Infinity>());
        errors1.push_back(localErrors.lpNorm<1>()/discretization.interior().size());
    }
    hdf_out.writeDoubleArray("error1", errors1);
    hdf_out.writeDoubleArray("errorInf", errorsInf);
    hdf_out.writeDoubleArray("dx",dxs);
    hdf_out.closeFile();
}

void solveOp(int m, int seed){
    long double eps = 1e-10; //for floating comparison
    std::ostringstream strs;
    strs << "Data/solOpm" << m << "Seed" << seed << ".h5";
    HDF hdf_out(strs.str(), HDF::DESTROY);
    Range<double> errors1, errorsInf;
    BallShape<Vec2d> domain({0,0}, 1);
    Range<double> dxs;
    for (double dx = 0.002; dx <= 0.1; dx+=0.001){
        dxs.push_back(dx);
        auto discretization = domain.discretizeBoundaryWithStep(dx);
        GeneralFill<Vec2d> fill_engine;
        fill_engine.seed(seed);
        discretization.fill(fill_engine, dx);
        int n = (m+1)*(m+2); //Bayona recommendation
        FindClosest find_support(n);
        discretization.findSupport(find_support);
        int N = discretization.size();
        VectorXd fun = VectorXd(N); fun.setZero();
        Monomials<Vec2d> mon(m);
        RBFFD<Polyharmonic<double,3>,Vec2d,ScaleToFarthest> approx({},mon);
        auto storage = discretization.computeShapes(approx);
        auto op = storage.explicitOperators();

        for (int p : discretization.all()){
            double x = discretization.pos(p,0);
            double y = discretization.pos(p,1);
            fun(p) = f2(x,y);
        }
        

        VectorXd localErrors(N); localErrors.setZero();

        for (int i : discretization.interior()){
            double x = discretization.pos(i,0);
            double y = discretization.pos(i,1);
            double real = lapf2(x,y);
            localErrors(i) = op.lap(fun,i)-real;

        }

        errorsInf.push_back(localErrors.lpNorm<Infinity>());
        errors1.push_back(localErrors.lpNorm<1>()/discretization.interior().size());
    }
    hdf_out.writeDoubleArray("error1", errors1);
    hdf_out.writeDoubleArray("errorInf", errorsInf);
    hdf_out.writeDoubleArray("dx",dxs);
    hdf_out.closeFile();
}


int main(int argc, char *argv[]){
    int m = std::stoi(argv[1]);
    int seed = std::stoi(argv[2]);
    solve(m,seed);
    solveOp(m,seed);
    return 0;
}