#include <medusa/Medusa.hpp>
#include <Eigen/Sparse>
#include <iostream>

using namespace mm;
using namespace Eigen;

double f2(double x, double y, double R){
    return 1 + std::sin(4*R*x)+std::cos(3*R*x)+std::sin(2*R*y);
}
double lapf2(double x, double y, double R){
    return -16*R*R*std::sin(4*R*x) - 9*R*R*std::cos(3*R*x)-4*R*R*std::sin(2*R*y);
}


void solve(int m, int seed){
    long double eps = 1e-10; //for floating comparison
    std::ostringstream strs;
    strs << "Data/solRm" << m << "Seed" << seed << ".h5";
    HDF hdf_out(strs.str(), HDF::DESTROY);
    Range<double> errors1, errorsInf;
    BallShape<Vec2d> domain({0,0}, 1);
    double dx=0.05;
    Range<double> Rs;

    auto discretization = domain.discretizeBoundaryWithStep(dx);
    GeneralFill<Vec2d> fill_engine;
    fill_engine.seed(seed);
    discretization.fill(fill_engine, dx);
    int n = (m+1)*(m+2); //Bayona recommendation
    FindClosest find_support(n);
    discretization.findSupport(find_support);
    int N = discretization.size();
    Monomials<Vec2d> mon(m);
    RBFFD<Polyharmonic<double,3>,Vec2d,ScaleToFarthest> approx({},mon);
    for (double R = 0.001; R <= 1; R+=0.005){
        Rs.push_back(R);

        auto storage = discretization.computeShapes(approx);
        SparseMatrix<double, Eigen::RowMajor> M(N,N);
        VectorXd rhs(N); rhs.setZero();
        auto op = storage.implicitOperators(M,rhs);
        for (int p : discretization.interior()){
            double x = discretization.pos(p,0);
            double y = discretization.pos(p,1);
            op.lap(p) = lapf2(x,y,R);
        }
        for (int p : discretization.boundary()){
            double x = discretization.pos(p,0);
            double y = discretization.pos(p,1);
            op.value(p) = f2(x,y,R);
        }
        //Eigen::SparseLU<decltype(M), COLAMDOrdering<int>> solver;
        Eigen::BiCGSTAB<decltype(M), IncompleteLUT<double>> solver;
        solver.preconditioner().setDroptol(1e-5);
        solver.preconditioner().setFillfactor(50);
        solver.setMaxIterations(200);
        solver.setTolerance(1e-14);
        solver.compute(M);
        VectorXd u = solver.solve(rhs);
        if (solver.info() != Eigen::ComputationInfo::Success)
            std::cout << "Solver didn't converge!" << std::endl;
        VectorXd localErrors(N); localErrors.setZero();
        double error=0;
        for (int i : discretization.interior()){
            double x = discretization.pos(i,0);
            double y = discretization.pos(i,1);
            localErrors(i) = u(i)-f2(x,y,R);
        }
        errorsInf.push_back(localErrors.lpNorm<Infinity>());
        errors1.push_back(localErrors.lpNorm<1>()/discretization.interior().size());
    }
    hdf_out.writeDoubleArray("error1", errors1);
    hdf_out.writeDoubleArray("errorInf", errorsInf);
    hdf_out.writeDoubleArray("R",Rs);
    hdf_out.closeFile();
}

void solveOp(int m, int seed){
    long double eps = 1e-10; //for floating comparison
    std::ostringstream strs;
    strs << "Data/solROpm" << m << "Seed" << seed << ".h5";
    HDF hdf_out(strs.str(), HDF::DESTROY);
    Range<double> errors1, errorsInf;
    BallShape<Vec2d> domain({0,0}, 1);
    double dx = 0.05;
    Range<double> Rs;

    auto discretization = domain.discretizeBoundaryWithStep(dx);
    GeneralFill<Vec2d> fill_engine;
    fill_engine.seed(seed);
    discretization.fill(fill_engine, dx);
    int n = (m+1)*(m+2); //Bayona recommendation
    FindClosest find_support(n);
    discretization.findSupport(find_support);
    int N = discretization.size();
    Monomials<Vec2d> mon(m);
    RBFFD<Polyharmonic<double,3>,Vec2d,ScaleToFarthest> approx({},mon);
    auto storage = discretization.computeShapes(approx);
    auto op = storage.explicitOperators();

    for (double R = 0.001; R <= 1; R+=0.005){
        Rs.push_back(R);

        VectorXd fun = VectorXd(N); fun.setZero();

        for (int p : discretization.all()){
            double x = discretization.pos(p,0);
            double y = discretization.pos(p,1);
            fun(p) = f2(x,y,R);
        }
        

        VectorXd localErrors(N); localErrors.setZero();

        for (int i : discretization.interior()){
            double x = discretization.pos(i,0);
            double y = discretization.pos(i,1);
            double real = lapf2(x,y,R);
            localErrors(i) = op.lap(fun,i)-real;

        }

        errorsInf.push_back(localErrors.lpNorm<Infinity>());
        errors1.push_back(localErrors.lpNorm<1>()/discretization.interior().size());
    }
    hdf_out.writeDoubleArray("error1", errors1);
    hdf_out.writeDoubleArray("errorInf", errorsInf);
    hdf_out.writeDoubleArray("R",Rs);
    hdf_out.closeFile();
}


int main(int argc, char *argv[]){
    int m = std::stoi(argv[1]);
    int seed = std::stoi(argv[2]);
    solve(m,seed);
    solveOp(m,seed);
    return 0;
}