Here is all the code required to generate the paper figures.

### Requirements
- Python 3+ with Numpy, Scipy, h5py, Matplotlib (idk which version exactly just try running the code)
- [Medusa C++ Library](https://e6.ijs.si/medusa/wiki/index.php/Medusa)

### Instructions
First make a folder named "Data" in this directory. The .cpp files should be ran first in order to generate the .h5 files (which will go into the Data folder). 
These can then be used by generatePlots.py to produce the Figures.
Alternatively, run the bash script runAll.sh to have this all done automatically (You may need to adjust the paths to Medusa and hdf5 in the g++ call).
Modify appropriately if you are interested in running only a single script/generating only a single figure.