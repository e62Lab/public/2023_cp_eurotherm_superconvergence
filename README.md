### A superconvergence result in the RBF-FD method

This repository has all the code used to generate the [Eurotherm 2024](https://www.eurotherm2024.si/) Eurotherm 2024 conference paper.

#### Abstract

Radial Basis Function-generated Finite Differences (RBF-FD) is a meshless method that can be used to numerically solve partial differential equations.
The solution procedure consists of two steps. First, the differential operator is discretised on given scattered nodes and afterwards, 
a global sparse matrix is assembled and inverted to obtain an approximate solution. Focusing on Polyharmonic Splines as our Radial Basis Functions (RBFs) of choice, 
appropriately augmented with monomials, it is well known that the truncation error of the differential operator approximation is determined by the degree of monomial augmentation.
Naively, one might think that the solution error will have the same order of convergence. We present a superconvergence result that shows otherwise - for some augmentation degrees,
order of convergence is higher than expected.

#### Authors
- Andrej Kolar-Požun
- Mitja Jančič
- Gregor Kosec

#### Repository structure overview
- **Code** C++ and Python codes used to generate the data and the figures.
- **Figures** All the figures used in the paper.
- **Paper** All the LaTeX files used to generate the paper.



For any questions regarding the paper please contact andrej.pozun@ijs.si